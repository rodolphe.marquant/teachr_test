import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import HomeScreen from './pages/HomeScreen'


export default function App() {
  return (
    <HomeScreen />
  );
}
