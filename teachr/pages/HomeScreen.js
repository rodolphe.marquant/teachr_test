import React, { Component } from 'react';
import { Platform, View, ScrollView, Text, StatusBar, SafeAreaView } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import styles, { colors } from '../styles/HomeScreen.style';


const DATA = [{
  "name": "Oswell Klosterman",
  "education": "University of Dammam",
  "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
  "avatar": "https://randomuser.me/api/portraits/men/1.jpg"
}, {
  "name": "Lara Letherbury",
  "education": "Schiller International University, London",
  "description": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
  "avatar": "https://randomuser.me/api/portraits/women/2.jpg"
}, {
  "name": "Judas Freckingham",
  "education": "Delta State University",
  "description": "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
  "avatar": "https://randomuser.me/api/portraits/men/2.jpg"
}, {
  "name": "Hendrick Bloor",
  "education": "Ternopil State Ivan Pul'uj Technical University",
  "description": "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
  "avatar": "https://randomuser.me/api/portraits/women/3.jpg"
}, {
  "name": "Arnie Pitt",
  "education": "Carlos Albizu University",
  "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
  "avatar": "https://randomuser.me/api/portraits/men/3.jpg"
}, {
  "name": "Der Musslewhite",
  "education": "Thainguyen University of Agriculture and Forestry",
  "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.",
  "avatar": "https://randomuser.me/api/portraits/women/4.jpg"
}, {
  "name": "Lynn Malkinson",
  "education": "Dakota Wesleyan University",
  "description": "Phasellus in felis. Donec semper sapien a libero. Nam dui.",
  "avatar": "https://randomuser.me/api/portraits/men/4.jpg"
}, {
  "name": "Werner Percifull",
  "education": "Bidhan Chandra Agricultural University",
  "description": "Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.",
  "avatar": "https://randomuser.me/api/portraits/women/5.jpg"
}, {
  "name": "Sandie Deluze",
  "education": "Universidad Pontificia Comillas",
  "description": "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.",
  "avatar": "https://randomuser.me/api/portraits/men/5.jpg"
}, {
  "name": "Evelina Dreschler",
  "education": "North Ossetian State University",
  "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
  "avatar": "https://randomuser.me/api/portraits/women/6.jpg"
}]

export default class HomeScreen extends Component {

    constructor (props) {
        super(props);
        this.state = {
            activeSlide: 0
        };
    }

    _renderItem ({item, index}) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    carousel() {
        const { activeSlide } = this.state;

        return (
            <View style={styles.mainSlider}>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={DATA}
                  renderItem={this._renderItem}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  firstItem={0}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={0.7}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  autoplay={false}
                  onSnapToItem={(index) => this.setState({ activeSlide: index }) }
                />
            </View>
        );
    }


    render () {
        const example1 = this.carousel();
        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>

                    <View
                      style={styles.header}
                    >
                      <Text style={styles.title}>Teachr's favoris</Text>
                    </View>

                        { example1 }
                </View>
            </SafeAreaView>
        );
    }
}