import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from './HomeScreen.style';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.75;
const slideWidth = viewportWidth * 0.80;
const itemHorizontalMargin = viewportWidth * 0.03;

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const itemRadius = 8;
 
export default StyleSheet.create({
    slideInnerContainer: {
        width: itemWidth,
        height: slideHeight,
        paddingHorizontal: itemHorizontalMargin,
        paddingBottom: 18, // for shadow
        paddingTop: 18, // for shadow
    },
    shadow: {
        position: 'absolute',
        top: 18,
        left: itemHorizontalMargin,
        right: itemHorizontalMargin,
        bottom: 18,
        shadowColor: colors.black,
        shadowOpacity: 1,
        shadowOffset: { width: 10, height: 10 },
        shadowRadius: 15,
        borderRadius: itemRadius
    },
    slideContainer: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: itemRadius,
        elevation: 9,
        padding: 25,
    },
    image: {
        width:60,
        height:60,
        borderRadius:30
    },
    name: {
        fontSize: 18,
        letterSpacing: 0.2,
        fontFamily: "Roboto",
        marginLeft:10
    },
    subtitle: {
        marginTop: 0,
        color: colors.gray
    },
    text: {
        fontFamily: "sans-serif-medium"
    },
    button1: {
        backgroundColor: colors.blue, 
        height: 40,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        padding: 5
    },
    button2: {
        backgroundColor: 'white',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        padding: 5,
        borderColor: '#ffa100',
        borderWidth: 2
    },
    buttonText: {
        fontSize: 14,
        fontFamily: "sans-serif-medium",
        color: "white"
    },
    buttonText2: {
        fontSize: 14,
        fontFamily: "sans-serif-medium",
        color: "#ffa100"
    },
    /* 
    /    Grid Display
    */
    rowAvatar: {
        height:61,
        alignSelf: 'stretch',
        flexDirection:"row"
    },
    avatarContainer: {
        flex: 0.4,
        width: 100,
        alignSelf: 'stretch',
        paddingLeft: 5
    },
    nameContainer: {
        flex: 1,
        width: 100,
        alignSelf: 'stretch',
        justifyContent: 'center',
        paddingLeft: 5,
    },
  rowContent: {
        flex: 1,
        alignSelf: 'stretch',
        marginTop:15,
        padding: 5,
    },
    formationContainer: {
        flex: 0.3,
        alignSelf: 'stretch',
    },
    descriptionContainer: {
        flex: 0.7,
        alignSelf: 'stretch',
    },
    buttonContainer: {
        flex: 0.4,
        alignSelf: 'stretch',

    }
});
