import { StyleSheet, Dimensions, Platform, StatusBar } from 'react-native';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const sliderHeight = viewportHeight;
const headerHeight =  Math.round(0.15 * sliderHeight);

export const colors = {
    black: '#000000',
    gray: '#888888',
    background: 'white',
    blue: '#0071cc'
};


export default StyleSheet.create({
    safeArea: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0 
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    header: {
        backgroundColor: colors.blue,
        height: headerHeight,
        justifyContent: 'center',
        paddingLeft: 25,
    },
    title: {
        fontSize: 23,
        fontWeight: 'bold',
        color: "white",
        fontFamily: "Roboto"
    },
    mainSlider: {
        paddingVertical: 30
    },
});