import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Alert } from 'react-native';
import styles from '../styles/SliderEntry.style';

export default class SliderEntry extends Component {

    get avatar () {
        const { data: { avatar } } = this.props;

        return (
            <Image
              source  ={{ uri: avatar }}
              style={styles.image}        
            />
        );
    }

    render () {
        const { data: { name, education, description } } = this.props;

        return (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.slideInnerContainer}
              onPress={() => { Alert.alert('Page du Teach\'r') }}
              >
                <View style={styles.shadow} />

                <View style={styles.slideContainer}>
                    <View style={styles.rowAvatar}>
                      <View style={styles.avatarContainer}>
                          { this.avatar }
                      </View>
                      <View style={styles.nameContainer}>
                          <Text style={styles.name}>{ name }</Text>
                      </View>
                    </View>
                    <View style={styles.rowContent}>
                      <View style={styles.formationContainer}>
                        <Text style={styles.subtitle}>
                          Formation
                        </Text>
                        <Text style={styles.text}>
                          { education }
                        </Text>
                      </View>
                      <View style={styles.descriptionContainer}>
                        <Text style={styles.subtitle}>
                          Description
                        </Text>
                        <Text style={styles.text}>
                          { description }
                        </Text>
                      </View>
                    </View>
                      
                    <View style={styles.buttonContainer}>

                    <TouchableOpacity onPress = {() => { Alert.alert('Cours réservé.') }}>
                      <View style={styles.button1}>
                          <Text style={styles.buttonText}>Prendre un cours avec ce Teach'r</Text>
                      </View>
                  </TouchableOpacity>

                    <TouchableOpacity onPress = {() => { Alert.alert('Ce Teach\'r ne fait plus partie de vos favoris') }}>
                      <View style={styles.button2}>
                          <Text style={styles.buttonText2}>Retirer ce Teach'r de mes favoris</Text>
                      </View>
                  </TouchableOpacity>
                    </View>
                </View>

            </TouchableOpacity>
        );
    }
}