<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Teachr.
 * @ApiResource(
 *     collectionOperations={"post"},
 *     itemOperations={"get", "put"}
 * )
 * @ORM\Entity
 */
class Teachr
{
    /**
     * @var int The id of this book.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string First name of the Teachr.
     *
     * @ORM\Column
     */
    private $firstName;

    /**
     * @var \DateTimeInterface Date of Teachr's creation
     *
     * @ORM\Column(type="datetime")
     */
    public $creationDate;


    public function __construct()
    {
        $this->creationDate = new \DateTime('now');
    }


    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }
}