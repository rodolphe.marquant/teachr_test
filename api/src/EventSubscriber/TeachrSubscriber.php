<?php

// api/src/EventSubscriber/TeachrSubscriber.php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Teachr;

final class TeachrSubscriber implements EventSubscriberInterface {

    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {

        return [
            KernelEvents::VIEW => [
                'incrementStatistics', EventPriorities::POST_WRITE,
            ],
        ];
    }

    public function incrementStatistics(ViewEvent $event)
    {
        $teachr = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$teachr instanceof Teachr || Request::METHOD_POST !== $method) {
            return;
        }

        $connection = $this->em->getConnection();
        $sql = 'UPDATE statistics SET counter=counter+1';
        $statement = $connection->prepare($sql);
        $statement->execute();

    }

}  
